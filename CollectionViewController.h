//
//  CollectionViewController.h
//  FinalProject
//
//  Created by Jason Clinger on 3/21/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewCell.h"

@interface CollectionViewController : UICollectionViewController
{
    NSMutableArray* arrayOfMetals;
    NSData* data;
    UICollectionView* cView;
    id d;
}
@end
