//
//  AppDelegate.h
//  FinalProject
//
//  Created by Jason Clinger on 3/21/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

