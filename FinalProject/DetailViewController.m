//
//  DetailViewController.m
//  FinalProject
//
//  Created by Jason Clinger on 3/23/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array = [NSArray arrayWithObjects: @"Past Month", @"Past 3 Months", @"Past 6 Months", @"Past Year", nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    _metalName.text = preName;
    _spotBuy.text = preSpotPrice;
    
    //_roundName.text = preRoundName;
    _roundBuy.text = preRoundBuy;
    _roundSell.text = preRoundSell;
    
    //_eagleName.text = preEagleName;
    _eagleBuy.text = preEagleBuy;
    _eagleSell.text = preEagleSell;
    
    _mapleBuy.text = preMapleBuy;
    _mapleSell.text = preMapleSell;
    
    //_junkName.text = preJunkName;
    _junkBuy.text = preJunkBuy;
    _junkSell.text = preJunkSell;

    //_wonderImage.image = preImage;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return array.count;
}

// Thurs 3/24 left off here, finish pickerView setup, check dataSource / delegate for required functions


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
