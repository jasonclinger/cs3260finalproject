//
//  DetailViewController.h
//  FinalProject
//
//  Created by Jason Clinger on 3/23/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
{
    
    NSArray* array;
    
    @public NSString* preName;
    NSString* preSpotPrice;
    
    //NSString* preRoundName;
    NSString* preRoundBuy;
    NSString* preRoundSell;
    
    //NSString* preEagleName;
    NSString* preEagleBuy;
    NSString* preEagleSell;
    
    NSString* preMapleBuy;
    NSString* preMapleSell;
    
    //NSString* preJunkName;
    NSString* preJunkBuy;
    NSString* preJunkSell;
    UIImage* preImage;
    
}

//TODO Insert some logic if metal doesn't have
//certain fields, to leave blank, otherwise fill in
//with eagle/round etc. buy/sell entries


@property (weak, nonatomic) IBOutlet UILabel *metalName;
@property (weak, nonatomic) IBOutlet UILabel *spotBuy;
@property (weak, nonatomic) IBOutlet UILabel *mapleBuy;
@property (weak, nonatomic) IBOutlet UILabel *mapleSell;
@property (weak, nonatomic) IBOutlet UILabel *eagleName;
@property (weak, nonatomic) IBOutlet UILabel *eagleBuy;
@property (weak, nonatomic) IBOutlet UILabel *eagleSell;
@property (weak, nonatomic) IBOutlet UILabel *roundName;
@property (weak, nonatomic) IBOutlet UILabel *roundBuy;
@property (weak, nonatomic) IBOutlet UILabel *roundSell;
@property (weak, nonatomic) IBOutlet UILabel *junkName;
@property (weak, nonatomic) IBOutlet UILabel *junkBuy;
@property (weak, nonatomic) IBOutlet UILabel *junkSell;


@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;





@end
